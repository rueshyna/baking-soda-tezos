# Baking Soda For Tezos Ecosystem

This is the client of [baking soda](https://gitlab.com/9chapters/baking-soda) for indexing Tezos node, MyTezosBaker and TulipTools. There is pre-generated configurations and env for both cases (see the config in ./index_config). For the Tezos node configuration case, we analyzed 492000 blocks and use [this repo](https://gitlab.com/9chapters/baking-soda-tezos-config) help to customizing it.

By running `./index_config/run.sh` on the 12G ram / 12 core machine and dedicated database with 2 core pre processe, to index the full data from tezos node:

- mainnet takes around 15 hours
- alphanet takes around 5 hours

# Update
- updated the configurations and model for blocks 0 - 694618

# Quick Start

`Nix` and `stack` are required.


```
git clone git@gitlab.com:9chapters/baking-soda.git
git clone git@gitlab.com:9chapters/baking-soda-tezos.git

cd baking-soda-tezos
stack build

docker run -p 5433:5432 --rm --name baking-soda-tezos -e POSTGRES_PASSWORD=pw -e POSTGRES_DB=tezos -d postgres:10.7
export PGPASSWORD=pw

# deploy sql
cd index_config
bash ./index_config/deploy_sql.sh "host=0.0.0.0 dbname=tezos user=postgres port=5433"

# start crawler and indexer
bash run.sh
```

Based on oure db/tezos node/machine, we can pass different parameters into `run.sh`. The first two argument allow us can determine which blocks we want to start to process. For example, we want to start processing from block 13432th and we set `number of docs per page` is 200. Then, the `page` should be `67` (floor (13432 / 200)).

```
bash run.sh [page] [number of docs per page] [tezos rpc] [number of threads pre process] [connection string for writing db] [connection string for reading db]
```

# How to Use

Thre are 5 subcommand for crawling, analysing and indexing data:

```
baking-soda-tezos Crawl   # crawl the data back from tezos node
baking-soda-tezos Model   # analyze the crawled back data and generate the config
baking-soda-tezos Verify  # use to verify the syntax of config
baking-soda-tezos Gen     # generate the sql from config
baking-soda-tezos Parser  # index the data based on config
baking-soda-tezos PostprocessDB # for processing stats data
```

The usage of the client of baking soda. The `-i` is for different source. Right now, there are `tezos`, `mytezosbaker`, `tuliptoolstotal`(tuliptools total) and `tuliptoolscirculating`(tuliptools circulating).

```
Usage: baking-soda-tezos [-i|--id ID] [-c|--connectStr CONNECT_STR]
                         [-f|--config CONFIG] [-e|--env ENV] [-p|--page PAGE]
                         [-n|--number NUMBER] [-t|--trunk TRUNK]
                         [-s|--sleep-time-if-success SLEEP_TIME_IF_SUCCESS]
                         [-d|--sleep-time-if-faild SLEEP_TIME_IF_FAILD] COMMAND

Available options:
  -h,--help                Show this help text
  -i,--id ID               the name of source, default name: "tezos"
  -c,--connectStr CONNECT_STR
                           default connect string: "host=0.0.0.0 dbname=crawler
                           user=postgres port=5433"
  -f,--config CONFIG       default config file: config
  -e,--env ENV             default env file: env
  -p,--page PAGE           Nth page from source. default: "0"
  -n,--number NUMBER       number of record pre page. default: "50"
  -t,--trunk TRUNK         size of trunk for inserting. default: "50"
  -s,--sleep-time-if-success SLEEP_TIME_IF_SUCCESS
                           sleep time if operation success. default: "0"
  -d,--sleep-time-if-faild SLEEP_TIME_IF_FAILD
                           sleep time if operation failed. default: "10000000"
  COMMAND                  Use '--help' to see more detail

Available commands:
  Crawl
  Model
  Verify
  Gen
  Parser
  PostprocessDB
```

## Crawl

The `Crawl` will crawl the block information from giving Tezos RPC entry and save the JSON in `raw_data` table. It will start to crawl from block 0. When it reached the head of block and go to crawl the next, it will wait until the next head come up.

## Model

This command will base on data in `raw_data` table to produce two file `config` and `env`. You can specify the number of blocks for modeling. The `env` is for `baking-soda-tezos` so DO NOT modify it. The `config` file is the setting to tell how to general the SQL schema and index data into database. Each of row in config is called rule.  The following details the config.

### Id
`Id` is serial number of rules.

### IsIndex
`Ture` for creating index when generated the SQL schema. Otherwise, `False`.

### Group
The `Group` describe the patterns which baking-soda detect. Considering the following 6 rules and 2 groups. Each of group represents different JSON structure.

```
Group      Path
[Group:0]  "header/level"
[Group:0]  "header/operations_hash"
[Group:0]  "header/proof_of_work_nonce"
[Group:0]  "header/signature
[Group:1]  "header/level"
[Group:1]  "header/operations_hash"
```

The `Group:1` represents the JSON for

```
{ "header":
  { "level" : "level value"
  , "operations_hash" : "operations_hash value"
  , "proof_of_work_nonce" : "proof_of_work_nonce value"
  , "signature" : "signature value"
  }
}
```

The `Group:2` represents the JSON for

```
{ "header":
  { "level" : "level value"
  , "operations_hash" : "operations_hash value"
  }
}
```

### Parent
Not Implement yet. The dependence of each rule.

### ModifyRule
Not fully implement. This column should be `Expand` in general but if the path is end with digits which mean JSON array, the value should be List.

### Type
To describe the type of the SQL schema. The mapping between the baking-soda type, the SQL type and Haskell data type.

Type                     | SQL type            | Haskell data type
---                      | ---                 | ---
VDArray:\<element type\> | \<element type\>:[] | [ \<element type\> ]
VDText                   | character varying   | Text
VDNumber                 | float8              | Double
VDBool                   | bool                | Bool
VDNull                   | -                   | -

### Database
Not Implement yet.

### Table
To describe which table should be stored the data.

### Column
To describe which column should be stored the data.

### Path
To describe the path of value. This column is for human to read only. For example, the path, "header/context", is for value, "targeted value".

```
{ "header":
  { "context" : "targeted value" }
}
```

The digits between the path are for describe JSON element in an array which is for `env`.

```
metadata/balance_updates/0/change
```

### UniqueKey
`UniqueKey` for adding `unique` keyword in the SQL schema.


### ReferenceDoc
To see where the rules is from. This column encodes by Base64. After decoding, it will be human readable format([Haskell data type](https://gitlab.com/9chapters/baking-soda-tezos/blob/dev/src/BakingSoda/Client/App/TezosBlock.hs#L34)). Taking the following as an example, this rule is from block level 467, 415, 379 and 368.


```
> echo WyJUQiB7bGV2ZWwgPSA0NjcsIGNoYWluSWQgPSBcIm1haW5cIn0iLCJUQiB7bGV2ZWwgPSA0MTUsIGNoYWluSWQgPSBcIm1haW5cIn0iLCJUQiB7bGV2ZWwgPSAzNzksIGNoYWluSWQgPSBcIm1haW5cIn0iLCJUQiB7bGV2ZWwgPSAzNjgsIGNoYWluSWQgPSBcIm1haW5cIn0iXQ== | base64 -D
["TB {level = 467, chainId = \"main\"}"
,"TB {level = 415, chainId = \"main\"}"
,"TB {level = 379, chainId = \"main\"}"
,"TB {level = 368, chainId = \"main\"}"]
```

### Rule
It's like Path but for baking-soda-tezos only. DO NOT modify.

## Verify
This command is to check configuration for correct syntax and indent configuration.

## Gen
The command will based on configuration to generate the SQL schema and the Haskell data type.

## Parser
The command will read both `config` and `env` file to parse the raw info from `raw_table`.

## PostprocessDB
This command is for postprocessing statistics data into `block_stats` table.

# TODO
see https://gitlab.com/9chapters/baking-soda

