module BakingSoda.Client.Arg where

import Options.Applicative
import Data.Monoid
import Data.String

defaultDBConnectStr :: String
defaultDBConnectStr = "host=0.0.0.0 dbname=crawler user=postgres port=5433"

defaultConfigFile :: String
defaultConfigFile = "config"

defaultEnvFile :: String
defaultEnvFile = "env"

defaultId :: String
defaultId = "tezos"

defaultDataType :: DataType
defaultDataType = SQLType

defaultPage :: Int
defaultPage = 0

defaultNumber :: Int
defaultNumber = 50

defaultTrunk :: Int
defaultTrunk = 50

defaultSleepS :: Int
defaultSleepS = 0

defaultSleepF :: Int
defaultSleepF = 10000000

defaultTezosNode :: String
defaultTezosNode = "rpc.tezrpc.me"

defaultBlocksPage :: Int
defaultBlocksPage = 0

defaultOpsPage :: Int
defaultOpsPage = 0

data Arg = Arg
         { id             :: Maybe String
         , connectStr     :: Maybe String
         , connectStrRead :: Maybe String
         , config         :: Maybe String
         , env            :: Maybe String
         , page           :: Maybe Int -- ^ nth page
         , number         :: Maybe Int -- ^ number of record pre page
         , trunk          :: Maybe Int -- ^ size of trunk for inserting
         , sleepS         :: Maybe Int -- ^ sleep time if operation success
         , sleepF         :: Maybe Int -- ^ sleep time if operation failed
         , commands       :: Command
         } deriving (Show)

data DataType = SQLType | HaskellType
              deriving (Show, Read)


data Command
    = Crawl
      { tezosNode :: Maybe String }
    | Model
    | Verify
    | Gen
      { datatype :: Maybe DataType }
    | Parser
    | PostprocessDB
    deriving (Show)

arg :: Parser Arg
arg = Arg
      <$> idArg
      <*> connectStrArg
      <*> connectStrReadArg
      <*> configFileArg
      <*> envFileArg
      <*> pageArg
      <*> numberArg
      <*> trunkArg
      <*> sleepSArg
      <*> sleepFArg
      <*> cmd

opStrOption :: (IsString s) => Mod OptionFields s -> Parser (Maybe s)
opStrOption = optional . strOption

opOption :: (Read s) => Mod OptionFields s -> Parser (Maybe s)
opOption = optional . option auto

sleepSArg:: Parser (Maybe Int)
sleepSArg = opOption
            $ mkFlag
                "sleep-time-if-success" 's' "SLEEP_TIME_IF_SUCCESS"
                ("sleep time if operation success. default: \"" <> show defaultSleepS <> "\"")

sleepFArg :: Parser (Maybe Int)
sleepFArg = opOption
            $ mkFlag
                "sleep-time-if-faild" 'd' "SLEEP_TIME_IF_FAILD"
                ("sleep time if operation failed. default: \"" <> show defaultSleepF <> "\"")

pageArg:: Parser (Maybe Int)
pageArg = opOption
            $ mkFlag
                "page" 'p' "PAGE"
                ("Nth page from source. default: \"" <> show defaultPage <> "\"")

numberArg:: Parser (Maybe Int)
numberArg = opOption
            $ mkFlag
                "number" 'n' "NUMBER"
                ("number of record pre page or number of blocks for Model command. default: \"" <> show defaultNumber <> "\"")

trunkArg:: Parser (Maybe Int)
trunkArg = opOption
            $ mkFlag
                "trunk" 't' "TRUNK"
                ("size of trunk for inserting. default: \"" <> show defaultTrunk <> "\"")

idArg :: Parser (Maybe String)
idArg = opStrOption
              $ mkFlag
                  "id" 'i' "ID"
                  ("the name of source, default name: \"" <> defaultId <> "\"")

connectStrArg :: Parser (Maybe String)
connectStrArg = opStrOption
              $ mkFlag
                  "connectStr" 'c' "CONNECT_STR"
                  ("default connect string: \"" <> defaultDBConnectStr <> "\"")

connectStrReadArg :: Parser (Maybe String)
connectStrReadArg = opStrOption
                  $ mkFlag
                  "connectStrRead" 'r' "CONNECT_STR_READ"
                  ("default connect string for read is as same as connect string")

configFileArg :: Parser (Maybe String)
configFileArg = opStrOption
              $ mkFlag
                  "config" 'f' "CONFIG"
                  ("default config file: " <> defaultConfigFile)

envFileArg :: Parser (Maybe String)
envFileArg = opStrOption
              $ mkFlag
                  "env"
                  'e'
                  "ENV"
                  ("default env file: " <> defaultEnvFile)


cmd :: Parser Command
cmd = subparser
                ( command "Crawl"
                    ( info (helper <*> crawlerCmd) fullDesc)
                <> command (show Model)
                    ( info (helper <*> modelCmd) fullDesc)
                <> command (show Verify)
                    ( info (helper <*> verifyCmd) fullDesc)
                <> command "Gen"
                    ( info (helper <*> genCmd) fullDesc)
                <> command ("Parser")
                    ( info (helper <*> parserCmd) fullDesc)
                <> command "PostprocessDB"
                    ( info (helper <*> postprocessDBCmd) fullDesc)
                <> help "Use '--help' to see more detail"
                )

crawlerCmd :: Parser Command
crawlerCmd = Crawl <$> tezosNodeCrawlCmd

tezosNodeCrawlCmd :: Parser (Maybe String)
tezosNodeCrawlCmd
  = opStrOption
  $ mkFlag
     "tezos-node-rpc" 'u' "TEZOS_NODE_RPC"
         ("rpc of tezos node, default rpc: \"" <> defaultTezosNode <> "\"")

modelCmd :: Parser Command
modelCmd = pure Model

verifyCmd :: Parser Command
verifyCmd = pure Verify

genCmd :: Parser Command
genCmd = Gen <$> dataTypeGenCmd

dataTypeGenCmd :: Parser (Maybe DataType)
dataTypeGenCmd
  = opOption
  $ mkFlag
     "data-type" 'a' "DataType"
         ("dataType, default data type: \"" <> show defaultDataType <> "\"")

parserCmd :: Parser Command
parserCmd = pure Parser

postprocessDBCmd :: Parser Command
postprocessDBCmd = pure PostprocessDB

blocksPagePostprocessCmd :: Parser (Maybe Int)
blocksPagePostprocessCmd = opOption
            $ mkFlag
                "ref block page" '1' "BLOCKS PAGE"
                ("Nth page from reference blocks table. default: \"" <> show defaultBlocksPage <> "\"")

opsPagePostprocessCmd :: Parser (Maybe Int)
opsPagePostprocessCmd = opOption
            $ mkFlag
                "ref op page" '2' "OPS PAGE"
                ("Nth page from reference ops table. default: \"" <> show defaultOpsPage <> "\"")

mkFlag :: (HasName f, HasMetavar f)
     => String  -- ^ long name
     -> Char    -- ^ short name
     -> String  -- ^ metavar
     -> String  -- ^ help text
     -> Mod f a
mkFlag l s m h = long l
             <> short s
             <> metavar m
             <> help h
