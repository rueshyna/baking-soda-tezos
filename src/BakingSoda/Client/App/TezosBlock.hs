{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings  #-}

module BakingSoda.Client.App.TezosBlock where

import Data.Typeable
import Data.Data
import Text.StringTemplate
import Text.StringTemplate.GenericStandard

import System.IO

import BakingSoda.Crawler
import BakingSoda.Model
import BakingSoda.Client.DB
import BakingSoda.Client.App.Common

import qualified Data.Set as S
import qualified Data.HashMap.Strict as H

---
{-
- Define Tezos Block Crawler
-}

data TzBlock = TB { level :: Int
                  , chainId :: String }
             deriving (Data, Typeable, Show, Read, Eq)

instance Iso TzBlock where
    zero _ = TB 0 "main"
    succ tb = tb { level = level tb + 1 }

tzBlock = TB { level = 0
             , chainId = "main"
             }

tzBlockRpc :: String -> String
tzBlockRpc tz = tz <> "/chains/$block.chainId$/blocks/$block.level$"

runTezosCrawler :: String -> String -> String -> Int -> Int -> Int -> Int -> String -> IO ()
runTezosCrawler id cs csr n t d1 d2 tz = do
    putStrLn "starting to run tezos crawler.."
    storeSnapshot 3 (Gain "block" tzBlock $ tzBlockRpc tz) n t d1 d2 id cs csr

---
{-
- Define Tezos Block Model
-}

stopRule :: [ Rule () ]
stopRule =
  [ Rule "" "operations" $ Rule "" "contents" $ Rule "" "metadata" $ Rule "" "operation_result" $ Rule "" "storage" $ Target ()
  , Rule "" "operations" $ Rule "" "contents" $ Rule "" "metadata" $ Rule "" "operation_result" $ Rule "" "errors" $ Target ()
  , Rule "" "operations" $ Rule "" "contents" $ Rule "" "metadata" $ Rule "" "operation_result" $ Rule "" "parameters" $ Target ()
  , Rule "" "operations" $ Rule "" "contents" $ Rule "" "metadata" $ Rule "" "operation_result" $ Rule "" "big_map_diff" $ Target ()
  , Rule "" "operations" $ Rule "" "contents" $ Rule "" "metadata" $ Rule "" "internal_operation_results" $ Rule "" "parameters" $ Target ()
  , Rule "" "operations" $ Rule "" "contents" $ Rule "" "metadata" $ Rule "" "internal_operation_results" $ Rule "" "result" $ Rule "" "storage" $ Target ()
  , Rule "" "operations" $ Rule "" "contents" $ Rule "" "metadata" $ Rule "" "internal_operation_results" $ Rule "" "result" $ Rule "" "big_map_diff" $ Target ()
  , Rule "" "operations" $ Rule "" "contents" $ Rule "" "metadata" $ Rule "" "internal_operation_results" $ Rule "" "result" $ Rule "" "errors" $ Target ()
  , Rule "" "operations" $ Rule "" "contents" $ Rule "" "parameters" $ Target ()
  , Rule "" "operations" $ Rule "" "contents" $ Rule "" "script" $ Target ()
  ]

buildTezosModel :: Int -> Int
  -> String -> String -> String -> String -> String ->IO ()
buildTezosModel n t = buildModel n 0 t (DS "tezos" "blocks") stopRule

---
{-
- Parser Block Json
-}
parserTezosBlockJson :: String -> String -> String -> String -> String -> Int -> Int -> Int -> Int -> Int -> IO ()
parserTezosBlockJson = parserJson stopRule

---
{-
- Postprocess Block
-}

postprocessTezosBlock :: String -> String -> IO ()
postprocessTezosBlock cs csr = do
    putStrLn "postprocessing..."
    simpleUpdateBlockInfosBHead cs csr
