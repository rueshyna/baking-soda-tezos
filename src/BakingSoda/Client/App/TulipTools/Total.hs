{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings  #-}

module BakingSoda.Client.App.TulipTools.Total where

import Data.Typeable
import Data.Data
import qualified Data.HashMap.Strict as H
import qualified Data.Time.Clock as TC

import BakingSoda.Crawler
import BakingSoda.Model
import BakingSoda.Client.App.Common
import BakingSoda.Client.App.TulipTools.Common

tuliptoolsTotal :: String
tuliptoolsTotal = "https://cmc.tulip.tools/total"

runTulipToolsTotalCrawler :: String -> String -> String -> Int -> Int -> Int -> Int -> String -> IO ()
runTulipToolsTotalCrawler =
    runTulipToolsCommonCrawler  "TulipToolsTotal" tuliptoolsTotal

buildTulipToolsTotalModel :: Int -> Int
  -> String -> String -> String -> String -> String -> IO ()
buildTulipToolsTotalModel =
    buildTulipToolsCommonModel (DS "tulip_tools" "tulip_tools_total")
