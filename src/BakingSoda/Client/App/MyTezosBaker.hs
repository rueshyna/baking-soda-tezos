{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE OverloadedStrings  #-}

module BakingSoda.Client.App.MyTezosBaker where

import Data.Typeable
import Data.Data
import qualified Data.HashMap.Strict as H
import qualified Data.Time.Clock as TC

import BakingSoda.Crawler
import BakingSoda.Model
import BakingSoda.Client.App.Common

---
{-
- Define MyTezosBaker Crawler
-}

data MyTezosBaker = MTB
                  { time :: TC.UTCTime }
             deriving (Data, Typeable, Show, Read, Eq)

instance Iso MyTezosBaker where
    zero = id
    succ mtb = MTB $ TC.addUTCTime sevenDays  (time mtb)

sevenDays :: TC.NominalDiffTime
sevenDays = 7 * TC.nominalDay

sleep7days :: Int
sleep7days = floor $ toRational sevenDays * 1000000

myTzBakerRpc :: String
myTzBakerRpc = "https://api.mytezosbaker.com/v1/bakers/"

runMyTezosBakerCrawler :: String -> String -> String -> Int -> Int -> Int -> Int -> String -> IO ()
runMyTezosBakerCrawler  id cs csr n t d1 d2 _ = do
    putStrLn "starting to run MyTezosBaker crawler.."
    currentTime <- TC.getCurrentTime
    let mtb = MTB { time = currentTime }
    storeSnapshot 3 (Gain "mytezosbaker" mtb myTzBakerRpc) n t sleep7days d2 id cs csr

---
{-
- Define MyTezosBaker Model
-}

stopRule :: [ Rule () ]
stopRule = []

buildMyTezosBakerModel :: Int -> Int
  -> String -> String -> String -> String -> String ->IO ()
buildMyTezosBakerModel n t = buildModel n 0 t (DS "tezos" "baker") stopRule

---
{-
- Parser Block Json
-}
parserMyTezosBakerBlockJson :: String -> String -> String -> String -> String -> Int -> Int -> Int -> Int -> Int -> IO ()
parserMyTezosBakerBlockJson = parserJson stopRule
