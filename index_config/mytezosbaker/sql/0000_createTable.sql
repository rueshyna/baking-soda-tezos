CREATE TABLE bakers (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "baker_name"           character varying,
    "delegation_code"      character varying unique,
    "fee"                  character varying,
    "logo"                 character varying);
CREATE INDEX "bakers_baker_name_idx" ON bakers ("baker_name");
CREATE INDEX "bakers_delegation_code_idx" ON bakers ("delegation_code");
CREATE INDEX "bakers_fee_idx" ON bakers ("fee");
CREATE INDEX "bakers_logo_idx" ON bakers ("logo");

