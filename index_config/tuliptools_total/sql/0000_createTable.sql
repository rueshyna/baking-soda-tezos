CREATE TABLE tulip_tools_total (
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "total"                float8,
    "uuid"                 character varying unique);
CREATE INDEX "tulip_tools_total_total_idx" ON tulip_tools_total ("total");
CREATE INDEX "tulip_tools_total_uuid_idx" ON tulip_tools_total ("uuid");

