CREATE TABLE block_stats (
     id SERIAL,
    "_inserted_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC'),
    "uuid"                 character varying unique,
    "level"                  float8 unique,
    "op_reveals_fee"         bigint,
    "op_delegations_fee"     bigint,
    "op_txs_fee"             bigint,
    "op_originations_fee"    bigint,
    "op_reveals_count"       bigint,
    "op_delegations_count"   bigint,
    "op_txs_count"           bigint,
    "op_originations_count"  bigint,
    "ops_count"              bigint,
    "op_txs_volume"          bigint
    );
CREATE INDEX "block_stats_level_idx" ON block_stats ("level");

