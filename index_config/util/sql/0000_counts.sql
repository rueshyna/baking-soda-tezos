START TRANSACTION;

CREATE TABLE row_counts
  ( relname  text PRIMARY KEY
  , reltuples   numeric
  , "updated_timestamp"  timestamp without time zone NOT NULL
        DEFAULT (current_timestamp AT TIME ZONE 'UTC')
  );

CREATE FUNCTION count_trig()
RETURNS TRIGGER AS
$$
  DECLARE
  BEGIN
    IF TG_OP = 'INSERT' THEN
      EXECUTE 'UPDATE row_counts set reltuples=reltuples +1, updated_timestamp = now() where relname = ''' || TG_RELNAME || '''';
      RETURN NEW;
    ELSIF TG_OP = 'DELETE' THEN
      EXECUTE 'UPDATE row_counts set reltuples=reltuples -1, updated_timestamp = now() where relname = ''' || TG_RELNAME || '''';
      RETURN OLD;
    END IF;
  END;
  $$
LANGUAGE 'plpgsql';


CREATE FUNCTION add_count_trigs()
RETURNS void AS
$$
   DECLARE
      rec   RECORD;
      q     text;
   BEGIN
      FOR rec IN SELECT relname
         FROM pg_class r JOIN pg_namespace n ON (relnamespace = n.oid)
         WHERE relkind = 'r' AND n.nspname = 'public' LOOP
            IF NOT EXISTS (SELECT * 
               FROM information_schema.triggers
               WHERE event_object_table = rec.relname
               AND trigger_name = rec.relname || '_count'
               ) THEN
                 q := 'CREATE CONSTRAINT TRIGGER ' || rec.relname || '_count AFTER INSERT OR DELETE ON ' ;
                 q := q || rec.relname || ' DEFERRABLE INITIALLY DEFERRED FOR EACH ROW EXECUTE PROCEDURE count_trig()';
                 EXECUTE q;
            END IF ;
      END LOOP;
   RETURN;
   END;
$$
LANGUAGE 'plpgsql';

CREATE FUNCTION init_row_counts()
RETURNS void AS
$$
   DECLARE
      rec   RECORD;
      crec  RECORD;
   BEGIN
      FOR rec IN SELECT relname
               FROM pg_class r JOIN pg_namespace n ON (relnamespace = n.oid)
               WHERE relkind = 'r' AND n.nspname = 'public' LOOP
         FOR crec IN EXECUTE 'SELECT count(*) as rows from '|| rec.relname LOOP
            -- nothing here, move along
         END LOOP;
         INSERT INTO row_counts values (rec.relname, crec.rows, now()) ;
      END LOOP;

   RETURN;
   END;
$$
LANGUAGE 'plpgsql';

SELECT init_row_counts();
SELECT add_count_trigs();

COMMIT;
