#!/bin/bash

CONN_STR=${1:-"host=0.0.0.0 dbname=tezos user=postgres port=5433"}

for i in `ls -1 tezos_block/sql/0* mytezosbaker/sql/0* tuliptools_total/sql/0* tuliptools_circulating/sql/0* util/sql/0*`
do
  echo "deploy .. " $i
  psql "${CONN_STR}" -f $i || exit 1
done
